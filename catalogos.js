var fs = require("fs");
var csv = require("fast-csv");
var redis  = require('redis');


var client = redis.createClient();
var filename=process.argv[2]
var deletedfile=process.argv[3]


if (deletedfile=="deleted") {
	console.log("Se elimina el archivo=>"+filename+".csv");
	client.keys(filename+'*', function (err, keys) {
		if (err) return console.log(err);
		for(var i = 0, len = keys.length; i < len; i++) {
			client.del(keys[i]);
		}
	});  
	return;
}

if (deletedfile=="deletedall") {
	console.log("Se eliminan todos los catalogos del SAT");
	client.keys('c_*', function (err, keys) {
		if (err) return console.log(err);
		for(var i = 0, len = keys.length; i < len; i++) {
			client.del(keys[i]);
		}
	});  
	return;
}

var stream = fs.createReadStream("catalogoscfdi/"+filename+".csv");
	console.log("Se carga el archivo =>"+filename+".csv");

var nitem=0;
var numero_campos=0;
var x=0;
var clave="";
var valor="";
var array_colname = new Array();

csv
 .fromStream(stream)
 .on("data", function(data){
		numero_campos=data.length;
		if(nitem==0){
			colname=new Array();
			for(a=0;a<data.length;a++){colname[a]=data[a];}
		}
		if (x==0) for (var i=0; i < numero_campos; i++){array_colname[i]= data[i];}
		
		var array_fila = new Array();
		var j=0;
		data.forEach(function(mov){
			array_fila[j]=mov;
			j++;
		});
		var arrayToJson = {};
		for(a=0;a<colname.length;a++){
			arrayToJson[colname[a]]=data[a];
		}
		var json = JSON.stringify(arrayToJson);
					if(nitem==0){
				clave=data[0];
				valor=data[1];
			}
		if (x!=0) {


			if(nitem!=0){
					client.hmset([filename+':'+data[0], clave, data[0], valor, data[1], 'valor', json]);
				console.log(clave+ data[0]+"::"+ valor+data[1]);
		}
		}
		x++;
		nitem++;
 })
 .on("end", function(){
  console.log("Hecho! se cargaron "+(nitem-1)+" filas");
  return;
 });
